﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Diagnostics;
using System.Globalization;

namespace PointerDetection
{
    public partial class MainForm : Form
    {

        const string APIKey = "PGaf6TVPRhJEoWrk4CfaM4q1uvqbm4t53qIK8CXQbTvYs1JEiR+rgVN7eUH64j+aIX6arWRcer0kxneKOwr7bw==";
        const string URI = "https://ussouthcentral.services.azureml.net/workspaces/a8f776344b654728974424fbc2b2da78/services/60bac888093247a7ae234becc9d2c4d7/execute?api-version=2.0&details=true";
        const int NUMOFTEST = 100;
        string filePath = "";
        private bool mouseDown;
        private Point lastLocation;

        Point PointRadnaPloha = new Point(307, 57);

        bool correctFilePath, correctRBChecked;

        Color myGreen = Color.FromArgb(102, 204, 153);
        Color myRed = Color.FromArgb(240, 52, 52);
        Color myGray = Color.FromArgb(80, 80, 80);
        Color myGray2 = Color.FromArgb(152, 152, 152);
        Color myGray3 = Color.FromArgb(190, 190, 190);
        Color defGRay = Color.FromKnownColor(KnownColor.AppWorkspace);
        Color myWhite = Color.Azure;

        bool workspaceSnimanjeOtvoreno, workspaceTestiranjeOtvoreno, workspaceInfoOtvoreno;
        int klasaM, klasaT;
        float[] aTocnost;
        bool showedAdvertise;


        bool capturingMouse, capturingEnabled;
        int countSampler;
        int[] samples;
        Point pntNew, pntOld;
        string samplesRow;
        bool testingMode;

        int countTestedTimes;
        string[] testedResults;


        public MainForm()
        {
            InitializeComponent();

            pnl_RadnaPloha_Default.Visible = true;
            pnl_RadnaPloha_Default.Location = new Point(PointRadnaPloha.X, PointRadnaPloha.Y);

            this.Size = new Size(1200, 650);
            capturingMouse = true;
            capturingEnabled = false;
            samples = new int[20];        

            testedResults = new string[NUMOFTEST];

            resetVariables();
            testingMode = false;
            countTestedTimes = 0;
            for (int i = 0; i < testedResults.Length; i++) testedResults[i] = "";


            correctFilePath = false;
            correctRBChecked = false;

            workspaceSnimanjeOtvoreno = false;
            workspaceTestiranjeOtvoreno = false;
            workspaceInfoOtvoreno = false;


            klasaM = 0;
            klasaT = 0;
            aTocnost = new float[NUMOFTEST];

            showedAdvertise = false;
        }

     
        //**** NASLOVNA TRAKA ****
        //*
        //*
        //* EXIT BUTTON *
        private void pb_ExitButton_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(1);
        }

        private void pb_ExitButton_MouseEnter(object sender, EventArgs e)
        {
            pb_ExitButton.Image = Properties.Resources.exit_button2;
        }

        private void pb_ExitButton_MouseLeave(object sender, EventArgs e)
        {
            pb_ExitButton.Image = Properties.Resources.exit_button;
        }


        //* MINIMIZE BUTTON *
        private void pb_minimizeButton_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }     

        private void pb_minimizeButton_MouseEnter(object sender, EventArgs e)
        {
            pb_minimizeButton.Image = Properties.Resources.minimize_button2;
        }

        private void pb_minimizeButton_MouseLeave(object sender, EventArgs e)
        {
            pb_minimizeButton.Image = Properties.Resources.minimize_button;

        }
      

        //* POMICANJE FORME *
        private void pnl_Naslov_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
            lastLocation = e.Location;
        }     

        private void pnl_Naslov_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                this.Location = new Point(
                    (this.Location.X - lastLocation.X) + e.X, (this.Location.Y - lastLocation.Y) + e.Y);

                this.Update();
            }
        }

        private void pnl_Naslov_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        




        //******* IZBORNIK *******
        //*
        //*
        //* SNIMI PODATKE *
        private void btn_Izbornik_SnimiPodatke_Click(object sender, EventArgs e)
        {
            //prikazi radnu plohu za snimanje podataka
            if(workspaceSnimanjeOtvoreno == false)
            {
                if (workspaceTestiranjeOtvoreno) sakrijRadnaPlohaTestirajPodatke();
                if (workspaceInfoOtvoreno) sakrijRadnaPlohaInfo();
                sakrijRadnaPlohaDefault();
                prikaziRadnaPlohaSnimiPodatke();
            }
            //sakrij radnu plohu i prikazi default plohu
            else
            {
                sakrijRadnaPlohaSnimiPodatke();
                prikaziRadnaPlohaDefault();
            }

        }

        //* TESTIRAJ PODATKE *
        private void btn_Izbornik_TestirajPodatke_Click(object sender, EventArgs e)
        {
            //prikazi radnu plohu za testiranje podataka
            if (workspaceTestiranjeOtvoreno == false)
            {
                if (workspaceSnimanjeOtvoreno) sakrijRadnaPlohaSnimiPodatke();
                if (workspaceInfoOtvoreno) sakrijRadnaPlohaInfo();
                sakrijRadnaPlohaDefault();
                prikaziRadnaPlohaTestirajPodatke();          
            }
            //sakrij radnu plohu i prikazi default plohu
            else
            {
                sakrijRadnaPlohaTestirajPodatke();
                prikaziRadnaPlohaDefault();
            }

        }

        //* INFO *
        private void btn_Izbornik_Info_Click(object sender, EventArgs e)
        {         
            //prikazi radnu plohu info
            if (workspaceInfoOtvoreno == false)
            {
                if (workspaceSnimanjeOtvoreno) sakrijRadnaPlohaSnimiPodatke();
                if (workspaceTestiranjeOtvoreno) sakrijRadnaPlohaTestirajPodatke();
                sakrijRadnaPlohaDefault();
                prikaziRadnaPlohaInfo();
            }
            //sakrij radnu plohu i prikazi default plohu
            else
            {
                sakrijRadnaPlohaInfo();
                prikaziRadnaPlohaDefault();
            }

        }

        //* MIJENJANJE IZMEĐU PROZORA *

        private void prikaziRadnaPlohaSnimiPodatke()
        {
            pnl_SnimiPodatke.BackColor = myGray;

            rb_IzvorMis.Checked = false;
            rb_IzvorTipkovnica.Checked = false;

            cbx_SnimiPodatke_Minimiziraj.Checked = false;
            cbx_SnimiPodatke_Otvori.Checked = false;

            workspaceSnimanjeOtvoreno = true;
            
            pnl_RadnaPloha_TesirajPodatke.Visible = false;
            pnl_RadnaPloha_SnimiPodatke.Visible = true;
            pnl_RadnaPloha_SnimiPodatke.Location = new Point(PointRadnaPloha.X, PointRadnaPloha.Y);


            correctRBChecked = false;
            pnl_UnderZapocniSnimanje.BackColor = myGray;
     
        }
        private void sakrijRadnaPlohaSnimiPodatke()
        {
            workspaceSnimanjeOtvoreno = false;
  
            pnl_RadnaPloha_SnimiPodatke.Visible = false;

            pnl_SnimiPodatke.BackColor = defGRay;
        }

        private void prikaziRadnaPlohaTestirajPodatke()
        {
            workspaceTestiranjeOtvoreno = true;
         
            pnl_RadnaPloha_TesirajPodatke.Visible = true;
            pnl_RadnaPloha_SnimiPodatke.Visible = false;
            pnl_RadnaPloha_TesirajPodatke.Location = new Point(PointRadnaPloha.X, PointRadnaPloha.Y);

            pnl_TestirajPodatke.BackColor = myGray;
        }
        private void sakrijRadnaPlohaTestirajPodatke()
        {
            workspaceTestiranjeOtvoreno = false;
 
            pnl_RadnaPloha_TesirajPodatke.Visible = false;

            pnl_TestirajPodatke.BackColor = defGRay;
        }

        private void prikaziRadnaPlohaInfo()
        {
            workspaceInfoOtvoreno = true;

            pnl_RadnaPloha_Info.Visible = true;
            pnl_RadnaPloha_SnimiPodatke.Visible = false;
            pnl_RadnaPloha_TesirajPodatke.Visible = false;

            pnl_RadnaPloha_Info.Location = new Point(PointRadnaPloha.X, PointRadnaPloha.Y);

            pnl_Info.BackColor = myGray;
        }
        private void sakrijRadnaPlohaInfo()
        {
            workspaceInfoOtvoreno = false;

            pnl_RadnaPloha_Info.Visible = false;

            pnl_Info.BackColor = defGRay;
        }

        private void prikaziRadnaPlohaDefault()
        {
            pnl_RadnaPloha_Default.Visible = true;
        }
        private void sakrijRadnaPlohaDefault()
        {
            pnl_RadnaPloha_Default.Visible = false;
        }



        //***** RADNA PLOHA *****
        //*
        //*
        //* SNIMI PODATKE *
        private void btn_PretraziFile_Click(object sender, EventArgs e)
        {                
            DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.

            //check if file is .txt file
            string temp = openFileDialog1.FileName, ext = "";
            for(int i = temp.Length-4; i<temp.Length; i++)
            {
                ext += temp[i];
            }

            if ((result == DialogResult.OK)&&(ext == ".txt")) // Test result.
            {
                filePath = openFileDialog1.FileName;
                lb_FilePath.Text = filePath;
                correctFilePath = true;
            }
            else if(ext != ".txt")
            {
                MessageBox.Show("Odaberite valjan format datoteke *.txt", "Pogreška");
            }

            //postavi boju ispod tipke za zapocinjanje
            if (correctFilePath && correctRBChecked)
            {
                //zelena
                pnl_UnderZapocniSnimanje.BackColor = myGreen;
            }

        }

        private void rb_IzvorMis_CheckedChanged(object sender, EventArgs e)
        {
            correctRBChecked = true;

            //postavi boju ispod tipke za zapocinjanje
            if (correctFilePath && correctRBChecked && (pnl_RadnaPloha_SnimiPodatke.Visible == true))
            {
                //zelena
                pnl_UnderZapocniSnimanje.BackColor = myGreen;
            }

        }

        private void rb_IzvorTipkovnica_CheckedChanged(object sender, EventArgs e)
        {
            correctRBChecked = true;

            //postavi boju ispod tipke za zapocinjanje
            if (correctFilePath && correctRBChecked && (pnl_RadnaPloha_SnimiPodatke.Visible == true))
            {
                //zelena
                pnl_UnderZapocniSnimanje.BackColor = myGreen;
            }

        }

        private void addCommasAndClass()
        {
            for (int i = 0; i < 20; i++)
            {
                samplesRow += samples[i] + ",";
            }
            if (rb_IzvorMis.Checked) samplesRow += "M";
            else samplesRow += "T";

        }

        private void writeToFile()
        {
            //bool foundDiffThanZero = false;
            int countDiffThanZero = 0;

            //provjera jesu li 10 i više podataka različiti od nula kako bi se zapisali u datoteku
            //nema smisla zapisivati ako je broj atributa jako malen
            for (int i = 0; i < samples.Length; i++)
            {
                if (samples[i] != 0)
                {
                    countDiffThanZero++;
                    if (countDiffThanZero >= 7) break;
                }
            }

            if (countDiffThanZero >= 7)
            {
                using (StreamWriter sw = File.AppendText(filePath))
                {
                    sw.WriteLine(samplesRow);
                }
            }

            resetVariables();
            testingMode = false;

        }

        private void btn_ZapočniSnimanjePodataka_Click(object sender, EventArgs e)
        {
            //zapocni jedino ako su ispravno odabrani rb i putanja od file-a
            if(correctRBChecked && correctFilePath)
            {

                //Snimanje će se zaustaviti
                if (capturingEnabled)
                {
                    capturingEnabled = false;
                    btn_ZapočniSnimanjePodataka.Text = "Pokreni snimanje podataka";

                    rb_IzvorMis.Enabled = true;
                    rb_IzvorTipkovnica.Enabled = true;

                    btn_Izbornik_Info.Enabled = true;
                    btn_Izbornik_SnimiPodatke.Enabled = true;
                    btn_Izbornik_TestirajPodatke.Enabled = true;

                    btn_PretraziFile.Enabled = true;
                    cbx_SnimiPodatke_Minimiziraj.Enabled = true;
                    cbx_SnimiPodatke_Otvori.Enabled = true;

                    tmr_Sampler.Stop();
                    resetVariables();

                    pnl_UnderZapocniSnimanje.BackColor = myGreen;

                    //otvori datoteku ako to korisnik zeli
                    try
                    {
                        if (cbx_SnimiPodatke_Otvori.Checked) Process.Start(filePath);
                    }
                    catch(Exception)
                    {
                        MessageBox.Show("Greška pri otvaranju datoteke", "Pogreška");
                    }

                }
                //Snimanje će se pokrenuti
                else
                {
                    pnl_UnderZapocniSnimanje.BackColor = myRed;

                    //minimiziraj ako to korisnik zeli
                    if(cbx_SnimiPodatke_Minimiziraj.Checked) this.WindowState = FormWindowState.Minimized;

                    capturingEnabled = true;
                    btn_ZapočniSnimanjePodataka.Text = "Zaustavi snimanje podataka";

                    rb_IzvorMis.Enabled = false;
                    rb_IzvorTipkovnica.Enabled = false;

                    btn_Izbornik_Info.Enabled = false;
                    btn_Izbornik_SnimiPodatke.Enabled = false;
                    btn_Izbornik_TestirajPodatke.Enabled = false;

                    btn_PretraziFile.Enabled = false;
                    cbx_SnimiPodatke_Minimiziraj.Enabled = false;
                    cbx_SnimiPodatke_Otvori.Enabled = false;


                    resetVariables();
                    tmr_Sampler.Start();
                }

                if (rb_IzvorMis.Checked) capturingMouse = true;
                else capturingMouse = false;


            }
        }
      


        //* TESTIRAJ PODATKE *
        //*
        //*
        private void btn_ZapocniTestiranjePodataka_Click(object sender, EventArgs e)
        {
            resetVariables();
            countTestedTimes = 0;

            //Testiranje će se zaustaviti
            if (testingMode)
            {
                testingMode = false;
                btn_ZapocniTestiranjePodataka.Text = "Pokreni testiranje";

                pnl_underZapocniTestiranjePodataka.BackColor = myGreen;

                btn_Izbornik_Info.Enabled = true;
                btn_Izbornik_SnimiPodatke.Enabled = true;
                btn_Izbornik_TestirajPodatke.Enabled = true;

                tmr_Sampler.Stop();

               
            }
            //Testiranje će se pokrenuti
            else
            {

                //testiranje
                testingMode = true;
                btn_ZapocniTestiranjePodataka.Text = "Zaustavi testiranje";

                pnl_underZapocniTestiranjePodataka.BackColor = myRed;

                btn_Izbornik_Info.Enabled = false;
                btn_Izbornik_SnimiPodatke.Enabled = false;
                btn_Izbornik_TestirajPodatke.Enabled = false;
               
                tmr_Sampler.Start();
            }
        }

        private void addToChart(string klasa, string tocnost)
        {
            float fTocnost;

            //float.TryParse(tocnost, out fTocnost);
            fTocnost = float.Parse(tocnost, CultureInfo.InvariantCulture);

            fTocnost *= 100;
         
            if (klasa == "M")
            {
                klasaM++;
                fTocnost = 100 - fTocnost;
             

            }else if(klasa == "T")
            {
                klasaT++;
            }

            int numOfSamples = klasaM + klasaT;
         
            aTocnost[numOfSamples - 1] = fTocnost;

            chartKlasa.Series["Klasa"].Points.Clear();
            chartKlasa.Series["Klasa"].Points.AddXY("Miš", klasaM);
            chartKlasa.Series["Klasa"].Points.AddXY("Tipkovnica", klasaT);

            chartTocnost.Series["Vjerojatnost"].Points.Clear();

            int i;
            if (numOfSamples > 10)
            {            
                for (i = numOfSamples - 10; i < numOfSamples; ++i)
                {
                    chartTocnost.Series["Vjerojatnost"].Points.AddY(aTocnost[i]);
                }

            }
            else
            {
                for (i =0; i < numOfSamples; ++i)
                {
                    chartTocnost.Series["Vjerojatnost"].Points.AddY(aTocnost[i]);
                }
            }
 
            //reklama
            float percentage = (float)klasaT / (float)numOfSamples;

            if(showedAdvertise== false && percentage >= 0.8 && numOfSamples>10)
            {
                showedAdvertise = true;
                showPopUp();
            }        
        }

        private void btn_resetirajGrafove_Click(object sender, EventArgs e)
        {
            resetirajGrafove();   
        }

        private void resetirajGrafove()
        {
            chartKlasa.Series["Klasa"].Points.Clear();
            chartTocnost.Series["Vjerojatnost"].Points.Clear();
            showedAdvertise = false;

            klasaM = 0;
            klasaT = 0;
            for(int i = 0; i < aTocnost.Length; i++)
            {
                aTocnost[i] = 0;
            }
        }

        public async Task InvokeRequestResponseService()
        {
            using (var client = new HttpClient())
            {
                var scoreRequest = new
                {

                    Inputs = new Dictionary<string, StringTable>() {
                        {
                            "input1",
                            new StringTable()
                            {
                                ColumnNames = new string[] {"Col1", "Col2", "Col3", "Col4", "Col5", "Col6", "Col7", "Col8", "Col9", "Col10", "Col11", "Col12", "Col13", "Col14", "Col15", "Col16", "Col17", "Col18", "Col19", "Col20", "Col21"},
                                Values = new string[,]   {{ samples[0].ToString(),
                                    samples[1].ToString(),
                                    samples[2].ToString(),
                                    samples[3].ToString(),
                                    samples[4].ToString(),
                                    samples[5].ToString(),
                                    samples[6].ToString(),
                                    samples[7].ToString(),
                                    samples[8].ToString(),
                                    samples[9].ToString(),
                                    samples[10].ToString(),
                                    samples[11].ToString(),
                                    samples[12].ToString(),
                                    samples[13].ToString(),
                                    samples[14].ToString(),
                                    samples[15].ToString(),
                                    samples[16].ToString(),
                                    samples[17].ToString(),
                                    samples[18].ToString(),
                                    samples[19].ToString(),
                                    "value" }, }
                            }
                        },
                    },
                    GlobalParameters = new Dictionary<string, string>()
                    {
                    }
                };
                const string apiKey = APIKey;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", apiKey);

                client.BaseAddress = new Uri(URI);


                HttpResponseMessage response = await client.PostAsJsonAsync("", scoreRequest);

                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsStringAsync();

                    
                    string rezultat = parseResult(result);
                    string klasa = rezultat[0].ToString();
                    string tocnost = "";
                    for(int i = 1; i < rezultat.Length; i++)
                    {
                        tocnost += rezultat[i].ToString();
                    }              
                  
                    addToChart(klasa, tocnost);
                    //MessageBox.Show(result + "    klasa="+klasa+ ", tocnost="+tocnost);
                }
                else
                {
                    string responseContent = await response.Content.ReadAsStringAsync();
                    MessageBox.Show(responseContent, "Fail");

                }
            }
        }

        private string parseResult(string rez)
        {

            for (int i = 0; i < rez.Length; i++)
            {
                if ((rez[i] == 'M') || ((rez[i] == 'T') && (rez[i + 1]) == '"'))
                {

                    testedResults[countTestedTimes] = rez[i].ToString();
                    countTestedTimes++;
                    string holder = rez[i].ToString(); ;

                    i += 4;
                    while (rez[i] != '"')
                    {
                        holder += rez[i];
                        i++;
                    }
                    //holder += rez[i + 4].ToString() + rez[i + 5].ToString() + rez[i + 6].ToString() + rez[i + 7].ToString() + rez[i + 8].ToString();
                    return holder;
                }
            }
            return "0";
        }

        private void showPopUp()
        {
            Form advertiseForm = new myPopUpForm();
            advertiseForm.Show();    
        }
       



        //****** OBOJE - SNIMI I TESTIRAJ*******
        //*
        //*

        public void resetVariables()
        {
            samplesRow = "";
            countSampler = 0;

            pntNew = Cursor.Position;
            pntOld = Cursor.Position;

            for (int i = 0; i < 20; i++) samples[i] = 0;

        }

        private void getSamples()
        {
            if (countSampler != 0) pntOld = pntNew;
            pntNew = Cursor.Position;

            samples[countSampler] = pntNew.X - pntOld.X;
            countSampler++;
            samples[countSampler] = pntNew.Y - pntOld.Y;
            countSampler++;
        }

        private void tmr_Sampler_Tick(object sender, EventArgs e)
        {
            if (countSampler < 20)
            {
                getSamples();
            }
            else if ((countSampler >= 20) && (testingMode == false))
            {
                tmr_Sampler.Stop();
                addCommasAndClass();
                writeToFile();
                tmr_Sampler.Start();
            }
            else if ((countSampler >= 20) && (testingMode == true))
            {
                tmr_Sampler.Stop();

                int countDiffThanZero = 0;

                //provjera jesu li 7 ili više podataka različiti od nula kako bi se testirali
                //nema smisla testirati ako je broj atributa jako malen
                for (int i = 0; i < samples.Length; i++)
                {
                    if (samples[i] != 0)
                    {
                        countDiffThanZero++;
                        if (countDiffThanZero >= 7) break;
                    }
                }

                if (countDiffThanZero >= 7)
                {
                    InvokeRequestResponseService();
                }

                resetVariables();
                testingMode = true;
                tmr_Sampler.Start();
             
               
            }

        }

    }

    public class StringTable
    {
        public string[] ColumnNames { get; set; }
        public string[,] Values { get; set; }
    }
}
