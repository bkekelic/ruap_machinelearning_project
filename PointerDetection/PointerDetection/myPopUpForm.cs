﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
namespace PointerDetection
{
    public partial class myPopUpForm : Form
    {
        const string mouse = "http://webshop.alles.hr/products/mis-ms-msi-blade-pro-gaming";
        const string webKatalog = "http://webshop.alles.hr/";


        public myPopUpForm()
        {
            InitializeComponent();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(webKatalog);
        }

      
        private void pb_Mouse_Click(object sender, EventArgs e)
        {
            Process.Start(mouse);

        }

        private void myPopUpForm_Load(object sender, EventArgs e)
        {
            this.Location = new Point(Screen.PrimaryScreen.Bounds.Width - this.Size.Width, Screen.PrimaryScreen.Bounds.Height - this.Size.Height - 40);
        }
    }
}
