﻿namespace PointerDetection
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tmr_Sampler = new System.Windows.Forms.Timer(this.components);
            this.pnl_Naslov = new System.Windows.Forms.Panel();
            this.pb_minimizeButton = new System.Windows.Forms.PictureBox();
            this.pb_ExitButton = new System.Windows.Forms.PictureBox();
            this.lb_Naslov_NazivApp = new System.Windows.Forms.Label();
            this.pnl_Izbornik = new System.Windows.Forms.Panel();
            this.pnl_Info = new System.Windows.Forms.Panel();
            this.btn_Izbornik_Info = new System.Windows.Forms.Button();
            this.pnl_TestirajPodatke = new System.Windows.Forms.Panel();
            this.btn_Izbornik_TestirajPodatke = new System.Windows.Forms.Button();
            this.pnl_SnimiPodatke = new System.Windows.Forms.Panel();
            this.btn_Izbornik_SnimiPodatke = new System.Windows.Forms.Button();
            this.pnl_RadnaPloha_SnimiPodatke = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.pnl_UnderZapocniSnimanje = new System.Windows.Forms.Panel();
            this.btn_ZapočniSnimanjePodataka = new System.Windows.Forms.Button();
            this.gb_SnimiPodatke_DodatnePostavke = new System.Windows.Forms.GroupBox();
            this.cbx_SnimiPodatke_Otvori = new System.Windows.Forms.CheckBox();
            this.cbx_SnimiPodatke_Minimiziraj = new System.Windows.Forms.CheckBox();
            this.gb_SnimiPodatke_OsnovnePostavke = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lb_FilePath = new System.Windows.Forms.Label();
            this.rb_IzvorTipkovnica = new System.Windows.Forms.RadioButton();
            this.btn_PretraziFile = new System.Windows.Forms.Button();
            this.rb_IzvorMis = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.pnl_RadnaPloha_TesirajPodatke = new System.Windows.Forms.Panel();
            this.btn_resetirajGrafove = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.chartTocnost = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chartKlasa = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label1 = new System.Windows.Forms.Label();
            this.pnl_underZapocniTestiranjePodataka = new System.Windows.Forms.Panel();
            this.btn_ZapocniTestiranjePodataka = new System.Windows.Forms.Button();
            this.pnl_RadnaPloha_Info = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.pnl_RadnaPloha_Default = new System.Windows.Forms.Panel();
            this.pnl_Naslov.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_minimizeButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_ExitButton)).BeginInit();
            this.pnl_Izbornik.SuspendLayout();
            this.pnl_RadnaPloha_SnimiPodatke.SuspendLayout();
            this.gb_SnimiPodatke_DodatnePostavke.SuspendLayout();
            this.gb_SnimiPodatke_OsnovnePostavke.SuspendLayout();
            this.pnl_RadnaPloha_TesirajPodatke.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartTocnost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartKlasa)).BeginInit();
            this.pnl_RadnaPloha_Info.SuspendLayout();
            this.SuspendLayout();
            // 
            // tmr_Sampler
            // 
            this.tmr_Sampler.Interval = 200;
            this.tmr_Sampler.Tick += new System.EventHandler(this.tmr_Sampler_Tick);
            // 
            // pnl_Naslov
            // 
            this.pnl_Naslov.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pnl_Naslov.Controls.Add(this.pb_minimizeButton);
            this.pnl_Naslov.Controls.Add(this.pb_ExitButton);
            this.pnl_Naslov.Controls.Add(this.lb_Naslov_NazivApp);
            this.pnl_Naslov.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_Naslov.Location = new System.Drawing.Point(0, 0);
            this.pnl_Naslov.Name = "pnl_Naslov";
            this.pnl_Naslov.Size = new System.Drawing.Size(1400, 50);
            this.pnl_Naslov.TabIndex = 7;
            this.pnl_Naslov.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnl_Naslov_MouseDown);
            this.pnl_Naslov.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnl_Naslov_MouseMove);
            this.pnl_Naslov.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pnl_Naslov_MouseUp);
            // 
            // pb_minimizeButton
            // 
            this.pb_minimizeButton.Image = global::PointerDetection.Properties.Resources.minimize_button;
            this.pb_minimizeButton.InitialImage = null;
            this.pb_minimizeButton.Location = new System.Drawing.Point(1111, 7);
            this.pb_minimizeButton.Name = "pb_minimizeButton";
            this.pb_minimizeButton.Size = new System.Drawing.Size(36, 36);
            this.pb_minimizeButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_minimizeButton.TabIndex = 2;
            this.pb_minimizeButton.TabStop = false;
            this.pb_minimizeButton.Click += new System.EventHandler(this.pb_minimizeButton_Click);
            this.pb_minimizeButton.MouseEnter += new System.EventHandler(this.pb_minimizeButton_MouseEnter);
            this.pb_minimizeButton.MouseLeave += new System.EventHandler(this.pb_minimizeButton_MouseLeave);
            // 
            // pb_ExitButton
            // 
            this.pb_ExitButton.Image = global::PointerDetection.Properties.Resources.exit_button;
            this.pb_ExitButton.InitialImage = null;
            this.pb_ExitButton.Location = new System.Drawing.Point(1156, 7);
            this.pb_ExitButton.Name = "pb_ExitButton";
            this.pb_ExitButton.Size = new System.Drawing.Size(36, 36);
            this.pb_ExitButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_ExitButton.TabIndex = 1;
            this.pb_ExitButton.TabStop = false;
            this.pb_ExitButton.Click += new System.EventHandler(this.pb_ExitButton_Click);
            this.pb_ExitButton.MouseEnter += new System.EventHandler(this.pb_ExitButton_MouseEnter);
            this.pb_ExitButton.MouseLeave += new System.EventHandler(this.pb_ExitButton_MouseLeave);
            // 
            // lb_Naslov_NazivApp
            // 
            this.lb_Naslov_NazivApp.AutoSize = true;
            this.lb_Naslov_NazivApp.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Naslov_NazivApp.ForeColor = System.Drawing.Color.Azure;
            this.lb_Naslov_NazivApp.Location = new System.Drawing.Point(9, 13);
            this.lb_Naslov_NazivApp.Name = "lb_Naslov_NazivApp";
            this.lb_Naslov_NazivApp.Size = new System.Drawing.Size(378, 26);
            this.lb_Naslov_NazivApp.TabIndex = 0;
            this.lb_Naslov_NazivApp.Text = "Detektiranje izvora kretnje pokazivača";
            // 
            // pnl_Izbornik
            // 
            this.pnl_Izbornik.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pnl_Izbornik.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_Izbornik.Controls.Add(this.pnl_Info);
            this.pnl_Izbornik.Controls.Add(this.btn_Izbornik_Info);
            this.pnl_Izbornik.Controls.Add(this.pnl_TestirajPodatke);
            this.pnl_Izbornik.Controls.Add(this.btn_Izbornik_TestirajPodatke);
            this.pnl_Izbornik.Controls.Add(this.pnl_SnimiPodatke);
            this.pnl_Izbornik.Controls.Add(this.btn_Izbornik_SnimiPodatke);
            this.pnl_Izbornik.Location = new System.Drawing.Point(0, 57);
            this.pnl_Izbornik.Name = "pnl_Izbornik";
            this.pnl_Izbornik.Size = new System.Drawing.Size(300, 586);
            this.pnl_Izbornik.TabIndex = 8;
            // 
            // pnl_Info
            // 
            this.pnl_Info.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.pnl_Info.Location = new System.Drawing.Point(3, 227);
            this.pnl_Info.Name = "pnl_Info";
            this.pnl_Info.Size = new System.Drawing.Size(10, 50);
            this.pnl_Info.TabIndex = 12;
            // 
            // btn_Izbornik_Info
            // 
            this.btn_Izbornik_Info.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.btn_Izbornik_Info.FlatAppearance.BorderSize = 0;
            this.btn_Izbornik_Info.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Izbornik_Info.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Izbornik_Info.ForeColor = System.Drawing.Color.Azure;
            this.btn_Izbornik_Info.Location = new System.Drawing.Point(13, 227);
            this.btn_Izbornik_Info.Name = "btn_Izbornik_Info";
            this.btn_Izbornik_Info.Size = new System.Drawing.Size(281, 50);
            this.btn_Izbornik_Info.TabIndex = 11;
            this.btn_Izbornik_Info.Text = "Info";
            this.btn_Izbornik_Info.UseVisualStyleBackColor = false;
            this.btn_Izbornik_Info.Click += new System.EventHandler(this.btn_Izbornik_Info_Click);
            // 
            // pnl_TestirajPodatke
            // 
            this.pnl_TestirajPodatke.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.pnl_TestirajPodatke.Location = new System.Drawing.Point(3, 155);
            this.pnl_TestirajPodatke.Name = "pnl_TestirajPodatke";
            this.pnl_TestirajPodatke.Size = new System.Drawing.Size(10, 50);
            this.pnl_TestirajPodatke.TabIndex = 10;
            // 
            // btn_Izbornik_TestirajPodatke
            // 
            this.btn_Izbornik_TestirajPodatke.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.btn_Izbornik_TestirajPodatke.FlatAppearance.BorderSize = 0;
            this.btn_Izbornik_TestirajPodatke.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Izbornik_TestirajPodatke.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Izbornik_TestirajPodatke.ForeColor = System.Drawing.Color.Azure;
            this.btn_Izbornik_TestirajPodatke.Location = new System.Drawing.Point(13, 155);
            this.btn_Izbornik_TestirajPodatke.Name = "btn_Izbornik_TestirajPodatke";
            this.btn_Izbornik_TestirajPodatke.Size = new System.Drawing.Size(281, 50);
            this.btn_Izbornik_TestirajPodatke.TabIndex = 9;
            this.btn_Izbornik_TestirajPodatke.Text = "Testiraj";
            this.btn_Izbornik_TestirajPodatke.UseVisualStyleBackColor = false;
            this.btn_Izbornik_TestirajPodatke.Click += new System.EventHandler(this.btn_Izbornik_TestirajPodatke_Click);
            // 
            // pnl_SnimiPodatke
            // 
            this.pnl_SnimiPodatke.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.pnl_SnimiPodatke.Location = new System.Drawing.Point(3, 82);
            this.pnl_SnimiPodatke.Name = "pnl_SnimiPodatke";
            this.pnl_SnimiPodatke.Size = new System.Drawing.Size(10, 50);
            this.pnl_SnimiPodatke.TabIndex = 1;
            // 
            // btn_Izbornik_SnimiPodatke
            // 
            this.btn_Izbornik_SnimiPodatke.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.btn_Izbornik_SnimiPodatke.FlatAppearance.BorderSize = 0;
            this.btn_Izbornik_SnimiPodatke.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Izbornik_SnimiPodatke.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Izbornik_SnimiPodatke.ForeColor = System.Drawing.Color.Azure;
            this.btn_Izbornik_SnimiPodatke.Location = new System.Drawing.Point(13, 82);
            this.btn_Izbornik_SnimiPodatke.Name = "btn_Izbornik_SnimiPodatke";
            this.btn_Izbornik_SnimiPodatke.Size = new System.Drawing.Size(281, 50);
            this.btn_Izbornik_SnimiPodatke.TabIndex = 0;
            this.btn_Izbornik_SnimiPodatke.Text = "Snimi podatke";
            this.btn_Izbornik_SnimiPodatke.UseVisualStyleBackColor = false;
            this.btn_Izbornik_SnimiPodatke.Click += new System.EventHandler(this.btn_Izbornik_SnimiPodatke_Click);
            // 
            // pnl_RadnaPloha_SnimiPodatke
            // 
            this.pnl_RadnaPloha_SnimiPodatke.BackgroundImage = global::PointerDetection.Properties.Resources.workspace_backgroud;
            this.pnl_RadnaPloha_SnimiPodatke.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_RadnaPloha_SnimiPodatke.Controls.Add(this.label4);
            this.pnl_RadnaPloha_SnimiPodatke.Controls.Add(this.pnl_UnderZapocniSnimanje);
            this.pnl_RadnaPloha_SnimiPodatke.Controls.Add(this.btn_ZapočniSnimanjePodataka);
            this.pnl_RadnaPloha_SnimiPodatke.Controls.Add(this.gb_SnimiPodatke_DodatnePostavke);
            this.pnl_RadnaPloha_SnimiPodatke.Controls.Add(this.gb_SnimiPodatke_OsnovnePostavke);
            this.pnl_RadnaPloha_SnimiPodatke.Location = new System.Drawing.Point(1321, 57);
            this.pnl_RadnaPloha_SnimiPodatke.Name = "pnl_RadnaPloha_SnimiPodatke";
            this.pnl_RadnaPloha_SnimiPodatke.Size = new System.Drawing.Size(885, 586);
            this.pnl_RadnaPloha_SnimiPodatke.TabIndex = 10;
            this.pnl_RadnaPloha_SnimiPodatke.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Azure;
            this.label4.Location = new System.Drawing.Point(686, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(180, 25);
            this.label4.TabIndex = 25;
            this.label4.Text = "Snimanje podataka";
            // 
            // pnl_UnderZapocniSnimanje
            // 
            this.pnl_UnderZapocniSnimanje.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.pnl_UnderZapocniSnimanje.Location = new System.Drawing.Point(289, 529);
            this.pnl_UnderZapocniSnimanje.Name = "pnl_UnderZapocniSnimanje";
            this.pnl_UnderZapocniSnimanje.Size = new System.Drawing.Size(281, 10);
            this.pnl_UnderZapocniSnimanje.TabIndex = 24;
            // 
            // btn_ZapočniSnimanjePodataka
            // 
            this.btn_ZapočniSnimanjePodataka.BackColor = System.Drawing.Color.Gray;
            this.btn_ZapočniSnimanjePodataka.FlatAppearance.BorderSize = 0;
            this.btn_ZapočniSnimanjePodataka.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ZapočniSnimanjePodataka.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ZapočniSnimanjePodataka.ForeColor = System.Drawing.Color.Azure;
            this.btn_ZapočniSnimanjePodataka.Location = new System.Drawing.Point(289, 479);
            this.btn_ZapočniSnimanjePodataka.Name = "btn_ZapočniSnimanjePodataka";
            this.btn_ZapočniSnimanjePodataka.Size = new System.Drawing.Size(281, 50);
            this.btn_ZapočniSnimanjePodataka.TabIndex = 23;
            this.btn_ZapočniSnimanjePodataka.Text = "Započni snimanje podataka";
            this.btn_ZapočniSnimanjePodataka.UseVisualStyleBackColor = false;
            this.btn_ZapočniSnimanjePodataka.Click += new System.EventHandler(this.btn_ZapočniSnimanjePodataka_Click);
            // 
            // gb_SnimiPodatke_DodatnePostavke
            // 
            this.gb_SnimiPodatke_DodatnePostavke.BackColor = System.Drawing.Color.Transparent;
            this.gb_SnimiPodatke_DodatnePostavke.Controls.Add(this.cbx_SnimiPodatke_Otvori);
            this.gb_SnimiPodatke_DodatnePostavke.Controls.Add(this.cbx_SnimiPodatke_Minimiziraj);
            this.gb_SnimiPodatke_DodatnePostavke.Location = new System.Drawing.Point(18, 289);
            this.gb_SnimiPodatke_DodatnePostavke.Name = "gb_SnimiPodatke_DodatnePostavke";
            this.gb_SnimiPodatke_DodatnePostavke.Size = new System.Drawing.Size(848, 120);
            this.gb_SnimiPodatke_DodatnePostavke.TabIndex = 22;
            this.gb_SnimiPodatke_DodatnePostavke.TabStop = false;
            this.gb_SnimiPodatke_DodatnePostavke.Text = "Dodatne postavke";
            // 
            // cbx_SnimiPodatke_Otvori
            // 
            this.cbx_SnimiPodatke_Otvori.AutoSize = true;
            this.cbx_SnimiPodatke_Otvori.BackColor = System.Drawing.Color.Transparent;
            this.cbx_SnimiPodatke_Otvori.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbx_SnimiPodatke_Otvori.ForeColor = System.Drawing.Color.Azure;
            this.cbx_SnimiPodatke_Otvori.Location = new System.Drawing.Point(47, 69);
            this.cbx_SnimiPodatke_Otvori.Name = "cbx_SnimiPodatke_Otvori";
            this.cbx_SnimiPodatke_Otvori.Size = new System.Drawing.Size(342, 24);
            this.cbx_SnimiPodatke_Otvori.TabIndex = 21;
            this.cbx_SnimiPodatke_Otvori.Text = "Otvori datoteku nakon završetka snimanja";
            this.cbx_SnimiPodatke_Otvori.UseVisualStyleBackColor = false;
            // 
            // cbx_SnimiPodatke_Minimiziraj
            // 
            this.cbx_SnimiPodatke_Minimiziraj.AutoSize = true;
            this.cbx_SnimiPodatke_Minimiziraj.BackColor = System.Drawing.Color.Transparent;
            this.cbx_SnimiPodatke_Minimiziraj.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbx_SnimiPodatke_Minimiziraj.ForeColor = System.Drawing.Color.Azure;
            this.cbx_SnimiPodatke_Minimiziraj.Location = new System.Drawing.Point(47, 39);
            this.cbx_SnimiPodatke_Minimiziraj.Name = "cbx_SnimiPodatke_Minimiziraj";
            this.cbx_SnimiPodatke_Minimiziraj.Size = new System.Drawing.Size(373, 24);
            this.cbx_SnimiPodatke_Minimiziraj.TabIndex = 20;
            this.cbx_SnimiPodatke_Minimiziraj.Text = "Minimiziraj sučelje nakon pokretanja snimanja";
            this.cbx_SnimiPodatke_Minimiziraj.UseVisualStyleBackColor = false;
            // 
            // gb_SnimiPodatke_OsnovnePostavke
            // 
            this.gb_SnimiPodatke_OsnovnePostavke.BackColor = System.Drawing.Color.Transparent;
            this.gb_SnimiPodatke_OsnovnePostavke.Controls.Add(this.label2);
            this.gb_SnimiPodatke_OsnovnePostavke.Controls.Add(this.lb_FilePath);
            this.gb_SnimiPodatke_OsnovnePostavke.Controls.Add(this.rb_IzvorTipkovnica);
            this.gb_SnimiPodatke_OsnovnePostavke.Controls.Add(this.btn_PretraziFile);
            this.gb_SnimiPodatke_OsnovnePostavke.Controls.Add(this.rb_IzvorMis);
            this.gb_SnimiPodatke_OsnovnePostavke.Controls.Add(this.label3);
            this.gb_SnimiPodatke_OsnovnePostavke.Location = new System.Drawing.Point(18, 42);
            this.gb_SnimiPodatke_OsnovnePostavke.Name = "gb_SnimiPodatke_OsnovnePostavke";
            this.gb_SnimiPodatke_OsnovnePostavke.Size = new System.Drawing.Size(848, 241);
            this.gb_SnimiPodatke_OsnovnePostavke.TabIndex = 21;
            this.gb_SnimiPodatke_OsnovnePostavke.TabStop = false;
            this.gb_SnimiPodatke_OsnovnePostavke.Text = "Osnovne postavke";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Azure;
            this.label2.Location = new System.Drawing.Point(25, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(141, 20);
            this.label2.TabIndex = 13;
            this.label2.Text = "Odaberi datoteku:";
            // 
            // lb_FilePath
            // 
            this.lb_FilePath.AutoSize = true;
            this.lb_FilePath.BackColor = System.Drawing.Color.Transparent;
            this.lb_FilePath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_FilePath.ForeColor = System.Drawing.Color.Azure;
            this.lb_FilePath.Location = new System.Drawing.Point(44, 78);
            this.lb_FilePath.Name = "lb_FilePath";
            this.lb_FilePath.Size = new System.Drawing.Size(0, 17);
            this.lb_FilePath.TabIndex = 14;
            // 
            // rb_IzvorTipkovnica
            // 
            this.rb_IzvorTipkovnica.AutoSize = true;
            this.rb_IzvorTipkovnica.BackColor = System.Drawing.Color.Transparent;
            this.rb_IzvorTipkovnica.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rb_IzvorTipkovnica.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rb_IzvorTipkovnica.Location = new System.Drawing.Point(47, 196);
            this.rb_IzvorTipkovnica.Name = "rb_IzvorTipkovnica";
            this.rb_IzvorTipkovnica.Size = new System.Drawing.Size(240, 21);
            this.rb_IzvorTipkovnica.TabIndex = 18;
            this.rb_IzvorTipkovnica.TabStop = true;
            this.rb_IzvorTipkovnica.Text = "Tipkovnica, daljinski upravljač i sl.";
            this.rb_IzvorTipkovnica.UseVisualStyleBackColor = false;
            this.rb_IzvorTipkovnica.CheckedChanged += new System.EventHandler(this.rb_IzvorTipkovnica_CheckedChanged);
            // 
            // btn_PretraziFile
            // 
            this.btn_PretraziFile.BackColor = System.Drawing.Color.Transparent;
            this.btn_PretraziFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_PretraziFile.Location = new System.Drawing.Point(172, 35);
            this.btn_PretraziFile.Name = "btn_PretraziFile";
            this.btn_PretraziFile.Size = new System.Drawing.Size(130, 30);
            this.btn_PretraziFile.TabIndex = 15;
            this.btn_PretraziFile.Text = "Pretraži";
            this.btn_PretraziFile.UseVisualStyleBackColor = false;
            this.btn_PretraziFile.Click += new System.EventHandler(this.btn_PretraziFile_Click);
            // 
            // rb_IzvorMis
            // 
            this.rb_IzvorMis.AutoSize = true;
            this.rb_IzvorMis.BackColor = System.Drawing.Color.Transparent;
            this.rb_IzvorMis.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rb_IzvorMis.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rb_IzvorMis.Location = new System.Drawing.Point(47, 164);
            this.rb_IzvorMis.Name = "rb_IzvorMis";
            this.rb_IzvorMis.Size = new System.Drawing.Size(258, 21);
            this.rb_IzvorMis.TabIndex = 17;
            this.rb_IzvorMis.TabStop = true;
            this.rb_IzvorMis.Text = "Miš, dodirna podloga (touchpad) i sl.";
            this.rb_IzvorMis.UseVisualStyleBackColor = false;
            this.rb_IzvorMis.CheckedChanged += new System.EventHandler(this.rb_IzvorMis_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Azure;
            this.label3.Location = new System.Drawing.Point(25, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(257, 20);
            this.label3.TabIndex = 16;
            this.label3.Text = "Odaberi izvor kretnje pokazivača:";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // pnl_RadnaPloha_TesirajPodatke
            // 
            this.pnl_RadnaPloha_TesirajPodatke.BackgroundImage = global::PointerDetection.Properties.Resources.workspace_backgroud;
            this.pnl_RadnaPloha_TesirajPodatke.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_RadnaPloha_TesirajPodatke.Controls.Add(this.btn_resetirajGrafove);
            this.pnl_RadnaPloha_TesirajPodatke.Controls.Add(this.label6);
            this.pnl_RadnaPloha_TesirajPodatke.Controls.Add(this.label5);
            this.pnl_RadnaPloha_TesirajPodatke.Controls.Add(this.chartTocnost);
            this.pnl_RadnaPloha_TesirajPodatke.Controls.Add(this.chartKlasa);
            this.pnl_RadnaPloha_TesirajPodatke.Controls.Add(this.label1);
            this.pnl_RadnaPloha_TesirajPodatke.Controls.Add(this.pnl_underZapocniTestiranjePodataka);
            this.pnl_RadnaPloha_TesirajPodatke.Controls.Add(this.btn_ZapocniTestiranjePodataka);
            this.pnl_RadnaPloha_TesirajPodatke.Location = new System.Drawing.Point(1290, 72);
            this.pnl_RadnaPloha_TesirajPodatke.Name = "pnl_RadnaPloha_TesirajPodatke";
            this.pnl_RadnaPloha_TesirajPodatke.Size = new System.Drawing.Size(885, 586);
            this.pnl_RadnaPloha_TesirajPodatke.TabIndex = 11;
            this.pnl_RadnaPloha_TesirajPodatke.Visible = false;
            // 
            // btn_resetirajGrafove
            // 
            this.btn_resetirajGrafove.BackColor = System.Drawing.Color.Gray;
            this.btn_resetirajGrafove.FlatAppearance.BorderSize = 0;
            this.btn_resetirajGrafove.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_resetirajGrafove.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_resetirajGrafove.ForeColor = System.Drawing.Color.Azure;
            this.btn_resetirajGrafove.Location = new System.Drawing.Point(627, 171);
            this.btn_resetirajGrafove.Name = "btn_resetirajGrafove";
            this.btn_resetirajGrafove.Size = new System.Drawing.Size(241, 45);
            this.btn_resetirajGrafove.TabIndex = 31;
            this.btn_resetirajGrafove.Text = "Resetiraj Grafove";
            this.btn_resetirajGrafove.UseVisualStyleBackColor = false;
            this.btn_resetirajGrafove.Click += new System.EventHandler(this.btn_resetirajGrafove_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Azure;
            this.label6.Location = new System.Drawing.Point(465, 228);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(309, 20);
            this.label6.TabIndex = 30;
            this.label6.Text = "Graf vjerojatnosti točne klasifikacije [%]:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Azure;
            this.label5.Location = new System.Drawing.Point(27, 228);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(173, 20);
            this.label5.TabIndex = 29;
            this.label5.Text = "Klasifikacijski graf [n]:";
            // 
            // chartTocnost
            // 
            this.chartTocnost.BackColor = System.Drawing.Color.Transparent;
            this.chartTocnost.BorderlineColor = System.Drawing.Color.Black;
            this.chartTocnost.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash;
            chartArea1.BackColor = System.Drawing.Color.Transparent;
            chartArea1.Name = "ChartArea1";
            this.chartTocnost.ChartAreas.Add(chartArea1);
            legend1.Alignment = System.Drawing.StringAlignment.Center;
            legend1.BackColor = System.Drawing.Color.Transparent;
            legend1.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            legend1.Name = "Legend1";
            this.chartTocnost.Legends.Add(legend1);
            this.chartTocnost.Location = new System.Drawing.Point(449, 250);
            this.chartTocnost.Name = "chartTocnost";
            series1.BorderWidth = 5;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            series1.LabelBackColor = System.Drawing.Color.Transparent;
            series1.Legend = "Legend1";
            series1.Name = "Vjerojatnost";
            this.chartTocnost.Series.Add(series1);
            this.chartTocnost.Size = new System.Drawing.Size(419, 300);
            this.chartTocnost.TabIndex = 28;
            this.chartTocnost.Text = "chart1";
            // 
            // chartKlasa
            // 
            this.chartKlasa.BackColor = System.Drawing.Color.Transparent;
            this.chartKlasa.BorderlineColor = System.Drawing.Color.Black;
            this.chartKlasa.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash;
            chartArea2.BackColor = System.Drawing.Color.Transparent;
            chartArea2.Name = "ChartArea1";
            this.chartKlasa.ChartAreas.Add(chartArea2);
            legend2.Alignment = System.Drawing.StringAlignment.Center;
            legend2.BackColor = System.Drawing.Color.Transparent;
            legend2.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            legend2.Name = "Legend1";
            this.chartKlasa.Legends.Add(legend2);
            this.chartKlasa.Location = new System.Drawing.Point(16, 250);
            this.chartKlasa.Name = "chartKlasa";
            series2.ChartArea = "ChartArea1";
            series2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            series2.LabelBackColor = System.Drawing.Color.Transparent;
            series2.Legend = "Legend1";
            series2.Name = "Klasa";
            this.chartKlasa.Series.Add(series2);
            this.chartKlasa.Size = new System.Drawing.Size(419, 300);
            this.chartKlasa.TabIndex = 12;
            this.chartKlasa.Text = "chart1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Azure;
            this.label1.Location = new System.Drawing.Point(684, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(184, 25);
            this.label1.TabIndex = 25;
            this.label1.Text = "Testiranje podataka";
            // 
            // pnl_underZapocniTestiranjePodataka
            // 
            this.pnl_underZapocniTestiranjePodataka.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(204)))), ((int)(((byte)(153)))));
            this.pnl_underZapocniTestiranjePodataka.Location = new System.Drawing.Point(289, 109);
            this.pnl_underZapocniTestiranjePodataka.Name = "pnl_underZapocniTestiranjePodataka";
            this.pnl_underZapocniTestiranjePodataka.Size = new System.Drawing.Size(281, 10);
            this.pnl_underZapocniTestiranjePodataka.TabIndex = 24;
            // 
            // btn_ZapocniTestiranjePodataka
            // 
            this.btn_ZapocniTestiranjePodataka.BackColor = System.Drawing.Color.Gray;
            this.btn_ZapocniTestiranjePodataka.FlatAppearance.BorderSize = 0;
            this.btn_ZapocniTestiranjePodataka.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ZapocniTestiranjePodataka.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ZapocniTestiranjePodataka.ForeColor = System.Drawing.Color.Azure;
            this.btn_ZapocniTestiranjePodataka.Location = new System.Drawing.Point(289, 59);
            this.btn_ZapocniTestiranjePodataka.Name = "btn_ZapocniTestiranjePodataka";
            this.btn_ZapocniTestiranjePodataka.Size = new System.Drawing.Size(281, 50);
            this.btn_ZapocniTestiranjePodataka.TabIndex = 23;
            this.btn_ZapocniTestiranjePodataka.Text = "Započni testiranje";
            this.btn_ZapocniTestiranjePodataka.UseVisualStyleBackColor = false;
            this.btn_ZapocniTestiranjePodataka.Click += new System.EventHandler(this.btn_ZapocniTestiranjePodataka_Click);
            // 
            // pnl_RadnaPloha_Info
            // 
            this.pnl_RadnaPloha_Info.BackgroundImage = global::PointerDetection.Properties.Resources.workspace_backgroud;
            this.pnl_RadnaPloha_Info.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_RadnaPloha_Info.Controls.Add(this.label12);
            this.pnl_RadnaPloha_Info.Controls.Add(this.label13);
            this.pnl_RadnaPloha_Info.Controls.Add(this.label10);
            this.pnl_RadnaPloha_Info.Controls.Add(this.label11);
            this.pnl_RadnaPloha_Info.Controls.Add(this.label8);
            this.pnl_RadnaPloha_Info.Controls.Add(this.label7);
            this.pnl_RadnaPloha_Info.Controls.Add(this.label9);
            this.pnl_RadnaPloha_Info.Location = new System.Drawing.Point(1274, 88);
            this.pnl_RadnaPloha_Info.Name = "pnl_RadnaPloha_Info";
            this.pnl_RadnaPloha_Info.Size = new System.Drawing.Size(885, 586);
            this.pnl_RadnaPloha_Info.TabIndex = 12;
            this.pnl_RadnaPloha_Info.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Azure;
            this.label12.Location = new System.Drawing.Point(13, 426);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(874, 120);
            this.label12.TabIndex = 31;
            this.label12.Text = resources.GetString("label12.Text");
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Azure;
            this.label13.Location = new System.Drawing.Point(13, 398);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(233, 24);
            this.label13.TabIndex = 30;
            this.label13.Text = "TESTIRANJE PODATAKA";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Azure;
            this.label10.Location = new System.Drawing.Point(13, 245);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(873, 120);
            this.label10.TabIndex = 29;
            this.label10.Text = resources.GetString("label10.Text");
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Azure;
            this.label11.Location = new System.Drawing.Point(13, 217);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(213, 24);
            this.label11.TabIndex = 28;
            this.label11.Text = "SNIMANJE PODATAKA";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Azure;
            this.label8.Location = new System.Drawing.Point(13, 42);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(845, 144);
            this.label8.TabIndex = 27;
            this.label8.Text = resources.GetString("label8.Text");
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Azure;
            this.label7.Location = new System.Drawing.Point(13, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 24);
            this.label7.TabIndex = 26;
            this.label7.Text = "OPIS";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Azure;
            this.label9.Location = new System.Drawing.Point(665, 15);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(200, 25);
            this.label9.TabIndex = 25;
            this.label9.Text = "Informacije o aplikaciji";
            // 
            // pnl_RadnaPloha_Default
            // 
            this.pnl_RadnaPloha_Default.BackgroundImage = global::PointerDetection.Properties.Resources.workspace_backgroud;
            this.pnl_RadnaPloha_Default.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_RadnaPloha_Default.Location = new System.Drawing.Point(307, 57);
            this.pnl_RadnaPloha_Default.Name = "pnl_RadnaPloha_Default";
            this.pnl_RadnaPloha_Default.Size = new System.Drawing.Size(885, 586);
            this.pnl_RadnaPloha_Default.TabIndex = 13;
            this.pnl_RadnaPloha_Default.Visible = false;
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ClientSize = new System.Drawing.Size(1400, 650);
            this.Controls.Add(this.pnl_RadnaPloha_Info);
            this.Controls.Add(this.pnl_RadnaPloha_TesirajPodatke);
            this.Controls.Add(this.pnl_RadnaPloha_Default);
            this.Controls.Add(this.pnl_RadnaPloha_SnimiPodatke);
            this.Controls.Add(this.pnl_Izbornik);
            this.Controls.Add(this.pnl_Naslov);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Detektiranje izvora kretnje pokazivača";
            this.pnl_Naslov.ResumeLayout(false);
            this.pnl_Naslov.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_minimizeButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_ExitButton)).EndInit();
            this.pnl_Izbornik.ResumeLayout(false);
            this.pnl_RadnaPloha_SnimiPodatke.ResumeLayout(false);
            this.pnl_RadnaPloha_SnimiPodatke.PerformLayout();
            this.gb_SnimiPodatke_DodatnePostavke.ResumeLayout(false);
            this.gb_SnimiPodatke_DodatnePostavke.PerformLayout();
            this.gb_SnimiPodatke_OsnovnePostavke.ResumeLayout(false);
            this.gb_SnimiPodatke_OsnovnePostavke.PerformLayout();
            this.pnl_RadnaPloha_TesirajPodatke.ResumeLayout(false);
            this.pnl_RadnaPloha_TesirajPodatke.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartTocnost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartKlasa)).EndInit();
            this.pnl_RadnaPloha_Info.ResumeLayout(false);
            this.pnl_RadnaPloha_Info.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer tmr_Sampler;
        private System.Windows.Forms.Panel pnl_Naslov;
        private System.Windows.Forms.Label lb_Naslov_NazivApp;
        private System.Windows.Forms.PictureBox pb_ExitButton;
        private System.Windows.Forms.PictureBox pb_minimizeButton;
        private System.Windows.Forms.Panel pnl_Izbornik;
        private System.Windows.Forms.Button btn_Izbornik_SnimiPodatke;
        private System.Windows.Forms.Panel pnl_SnimiPodatke;
        private System.Windows.Forms.Panel pnl_Info;
        private System.Windows.Forms.Button btn_Izbornik_Info;
        private System.Windows.Forms.Panel pnl_TestirajPodatke;
        private System.Windows.Forms.Button btn_Izbornik_TestirajPodatke;
        private System.Windows.Forms.Panel pnl_RadnaPloha_SnimiPodatke;
        private System.Windows.Forms.Button btn_PretraziFile;
        private System.Windows.Forms.Label lb_FilePath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.RadioButton rb_IzvorTipkovnica;
        private System.Windows.Forms.RadioButton rb_IzvorMis;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox gb_SnimiPodatke_OsnovnePostavke;
        private System.Windows.Forms.CheckBox cbx_SnimiPodatke_Minimiziraj;
        private System.Windows.Forms.GroupBox gb_SnimiPodatke_DodatnePostavke;
        private System.Windows.Forms.CheckBox cbx_SnimiPodatke_Otvori;
        private System.Windows.Forms.Button btn_ZapočniSnimanjePodataka;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel pnl_UnderZapocniSnimanje;
        private System.Windows.Forms.Panel pnl_RadnaPloha_TesirajPodatke;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnl_underZapocniTestiranjePodataka;
        private System.Windows.Forms.Button btn_ZapocniTestiranjePodataka;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartKlasa;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartTocnost;
        private System.Windows.Forms.Button btn_resetirajGrafove;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel pnl_RadnaPloha_Info;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel pnl_RadnaPloha_Default;
    }
}

